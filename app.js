// node server setup imports:
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var configDB = require('./config/database.js');

app.use(express.static('public'));

//run server on port 3000
var port     = process.env.PORT || 3000;

//config mongoDB 
mongoose.connect(configDB.url); // connect to our database


//req passport for login system
require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

//templating views with ejs 
app.set('view engine', 'ejs');
app.use('/styles', express.static('styles'));


app.use(session({
    secret: 'sess0secr3t', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// require routes so the app knows what view to render
require('./app/routes.js')(app, passport); 

// socket.io for real chat application
io.on('connection', function(socket){
	socket.on('chat message', function(msg){
        socket.emit('your message', msg);
		socket.broadcast.emit('chat message', msg);
	});
});

// start the server on the port that was setup earlier 3000 in our case
http.listen(port);
console.log('Server is running, run after it(this used to be a him but gpop aka gabby made a sexist joke about it an from now on we are not asuming the genred our matcha is for everybody not only for females as our colegue gabby told us) ' + port);