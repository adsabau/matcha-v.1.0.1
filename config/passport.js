// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var User       = require('../app/models/user');
var configAuth = require('./auth');

module.exports = function(passport) {

    // startup passport session
    
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // create new account
    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true //(lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email){
            email = email.toLowerCase(); 
        }

        process.nextTick(function() {
            User.findOne({ 'local.email' :  email }, function(err, user) {
                if (err){
                    return done(err);
                }

                if (!user){
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                }

                if (!user.validPassword(password)){
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                }
                else{
                    return done(null, user);
                }
            });
        });

    }));


    //singup newly created account 

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true //(lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email){
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
        }
        process.nextTick(function() {
            // if the user is not already logged in:
            if (!req.user) {
                User.findOne({ 'local.email' :  email }, function(err, user) {
                    // if there are any errors, return the error
                    if (err){
                        return done(err);
                    }
                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        req.body.login = req.body.login.toLowerCase();
                        User.findOne({ 'local.login' :  req.body.login }, function(err, user) {
                            // if there are any errors, return the error
                            if (err){
                                return done(err);
                            }
                            // check to see if theres already a user with that login
                            if (user) {
                                return done(null, false, req.flash('signupMessage', 'That login is already taken.'));
                            } else {
                                // create the user
                                var newUser            = new User();

                                newUser.local.email    = email;
                                newUser.local.login    = req.body.login;
                                newUser.local.name     = req.body.name;
                                newUser.local.rating     = 0;
                                newUser.local.password = newUser.generateHash(password);

                                newUser.save(function(err) {
                                    if (err){
                                        return done(err);
                                    }
                                    return done(null, newUser);
                                });
                            }
                        });
                    }

                });
            } else if ( !req.user.local.email ) {
                //check if the emai lis used by another user or not
                User.findOne({ 'local.email' :  email }, function(err, user) {
                    if (err){
                        return done(err);
                    }
                    if (user) {
                        return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                    } else {
                        var user = req.user;
                        user.local.email = email;
                        user.local.password = user.generateHash(password);
                        user.save(function (err) {
                            if (err){
                                return done(err);
                            }
                            return done(null,user);
                        });
                    }
                });
            } else {
                return done(null, req.user);
            }

        });

    }));

    // FACEBOOK connection via passport (ask google i have no idea what i'm doing here)

    var fbStrategy = configAuth.facebookAuth;
    fbStrategy.passReqToCallback = true;  
    passport.use(new FacebookStrategy(fbStrategy,
    function(req, token, refreshToken, profile, done) {

        process.nextTick(function() {

            if (!req.user) {

                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                            user.save(function(err) {
                                if (err){
                                    return done(err);
                                }
                                return done(null, user);
                            });
                        }
                        return done(null, user);
                    } else {
                        var newUser            = new User();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = (profile.emails[0].value || '').toLowerCase();

                        newUser.save(function(err) {
                            if (err){
                                return done(err);
                            }
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                var user            = req.user; 

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                user.save(function(err) {
                    if (err)
                        return done(err);
                        
                    return done(null, user);
                });

            }
        });

    }));

    // G+ like previous FACEBOOK 
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true 
    },
    function(req, token, refreshToken, profile, done) {

        process.nextTick(function() {

            if (!req.user) {

                User.findOne({ 'google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.google.token) {
                            user.google.token = token;
                            user.google.name  = profile.displayName;
                            user.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                            user.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        var newUser          = new User();

                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                        newUser.save(function(err) {
                            if (err)
                                return done(err);
                                
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                var user               = req.user; // pull the user out of the session

                user.google.id    = profile.id;
                user.google.token = token;
                user.google.name  = profile.displayName;
                user.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                user.save(function(err) {
                    if (err)
                        return done(err);
                        
                    return done(null, user);
                });

            }

        });

    }));

};
